import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeekWheaterComponent } from './week-wheater.component';

describe('WeekWheaterComponent', () => {
  let component: WeekWheaterComponent;
  let fixture: ComponentFixture<WeekWheaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeekWheaterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeekWheaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
