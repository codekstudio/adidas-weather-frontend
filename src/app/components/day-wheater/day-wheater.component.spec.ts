import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DayWheaterComponent } from './day-wheater.component';

describe('DayWheaterComponent', () => {
  let component: DayWheaterComponent;
  let fixture: ComponentFixture<DayWheaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DayWheaterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DayWheaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
