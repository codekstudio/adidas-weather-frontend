import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ForecastService {
  private url: string;

  constructor(private http: HttpClient) {
    this.url = `${environment.apiUrl}/`;
  }

  getForecastByCity(city: string): Observable<any> {
    const url = this.url + 'forecasts/' + city;

    return this.http.get(url).pipe(
      catchError(this.handleError),
    );
  }



  // Error handling

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else if (error.message) {
      console.log(error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    return throwError(
      'Something bad happened; please try again later.');
  };
}
