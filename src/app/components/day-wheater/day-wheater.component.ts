import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-day-wheater',
  templateUrl: './day-wheater.component.html',
  styleUrls: ['./day-wheater.component.scss']
})
export class DayWheaterComponent implements OnInit {

  @Input() daySelected: any;

  constructor() { }

  ngOnInit(): void {
  }

}
