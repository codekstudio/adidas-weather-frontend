import { Component, OnInit, OnDestroy } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  forecastDay: any;

  constructor() {}

  ngOnInit(): void {

  }

  selectedDay(row: any) {
    this.forecastDay = row;
  }

}
