import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ForecastService } from '../../services/forecast.service';
// import { Subscription } from 'rxjs';

@Component({
  selector: 'app-week-wheater',
  templateUrl: './week-wheater.component.html',
  styleUrls: ['./week-wheater.component.scss']
})
export class WeekWheaterComponent implements OnInit {
  displayedColumns: string[] = ['date', 'temp', 'main', 'description'];
  forecastWeek: any = []
  city: string = 'Zaragoza';

  @Output() selectedDay = new EventEmitter<any>();

  constructor(private readonly forecastService: ForecastService) { }

  ngOnInit(): void {
    
  }

  getForecast(city: string) {
    this.forecastService.getForecastByCity(city).subscribe(res => {
      if(res && res.forecasts && res.forecasts.length) {
        this.forecastWeek = res.forecasts;
      }
    }, err => console.error(err.message ? err.message : err));
  }

  selectDay(row: any) {
    console.log(row)
    this.selectedDay.emit(row);
  }

  onSubmit() {
    this.forecastWeek = [];
    this.getForecast(this.city);
  }

}
