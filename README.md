# Description

Angular v12.1.1 project for adidas weather app challenge.

This repository is part of the "adidas weather app" challenge. To see all project repositories go to https://bitbucket.org/codekstudio/workspace/projects/ADI

This repository is the frontend of the system and comprises the following main components:

* A "home" view to contain the main components.

* A component to list weekly predictions.

* A component for the daily predictions selected from the previous list.

* A form input to write the city from which the predictions are wanted.
## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## To be done

Due to lack of time the system is not complete. Some main elements to be completed are noted here:

* Timer to check the backend from time to time and check alerts for temperatures over 40ºC.

* Websocket: It is necessary to create the websocket and mqtt client to connect to the message broker (or directly to the backend with the websocket) and receive temperature alerts in real time.

* Services and components to list a range, save and delete elements from the historical database.